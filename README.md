# Aircraft panel generator

A Python script to make DXF CAD files for standard-sized aircraft panels.

Requires the [ezdxf package](https://pypi.org/project/ezdxf/)

To use this software, edit the 'Parameters' section in the script file and run it. The output is 'panel.dxf'. The script will overwrite 'panel.dxf' every time it is run, so move or rename it.